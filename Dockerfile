# Specify the base image and environment
ARG PYTHON_VERSION=3.12.2
FROM python:${PYTHON_VERSION}-slim as base

# Prevents Python from writing pyc files.
ENV PYTHONDONTWRITEBYTECODE=1

# Keeps Python from buffering stdout and stderr to avoid situations where
# the application crashes without emitting any logs due to buffering.
ENV PYTHONUNBUFFERED=1

# Set the working directory
WORKDIR /app

RUN apt-get update && apt-get install -y git pkg-config libpq-dev libgl1-mesa-glx libxrender1 xvfb
RUN git init
RUN python -m pip install flask-sock websocket-client
RUN python -m pip install flask-cors
COPY . .
RUN python -m pip install -e .
RUN python -m pip install docker

# Expose the port that the application listens on.
EXPOSE 8501
EXPOSE 5000

# install playwright dependencies and tools
CMD python streamlit_pyvista/server_managers/server_manager_proxified.py & streamlit-pyvista run proxy
#CMD python streamlit_pyvista/proxy.py & cd examples && streamlit run main.py
#