import streamlit as st

from streamlit_pyvista.mesh_viewer_component import MeshViewerComponent
from streamlit_pyvista.trame_viewers import get_advanced_viewer_path
from streamlit_pyvista.server_managers import ServerManagerProxified


def main():
    st.title("Streamlit-PyVista Example of different simple meshes")
    MeshViewerComponent(["assets/results_corr-000019.vtk"],
                        trame_viewer_class=get_advanced_viewer_path(),
                        server_manager_class=ServerManagerProxified).show()


if __name__ == "__main__":
    main()
