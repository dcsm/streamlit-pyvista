# Examples
In this folder you'll find 2 different examples.

## Simple example
you can run this example with the following command
````shell
streamlit run simple_example.py
````
You can see a simple web view with 2 different independent viewer displaying mesh from the assets folder. 
The 2 viewers are not similar, this is controlled by the `trame_viewer_class` argument that takes a path to the 
definition of a python class defining the viewer. By default there are 2 viewer that exists, the `TrameViewer`
and the `TrameAdvancedViewer`. If you wish to make a custom viewer you should consider taking those as starting point.

The 2 instances of `MeshViewerComonent` have also an argument specifying what "server manager" to use. The server manager
is the class in charge of spawning all the Trame servers, by default there exists a simple `ServerManager` that
is the one used by default and work out of the box if you use trame locally.
However, to use this library remotely on docker container for example there exists another one, the
`ServerManagerProxified`. This one create a proxy that route all the viewer through one port, making it easy to host 
since only the port for streamlit and for the proxy need to be open.


## Mesh Sequence example
you can run this example with the following command
````shell
streamlit run mesh_sequence.py
````
In this example you can see that if you pass a sequence of mesh, then you'll be able to cycle through them with a slider. 


The assets are coming from [jtcam-data-10172](https://gitlab.renkulab.io/guillaume.anciaux/jtcam-data-10172) 
and  [solidipes examples](https://gitlab.com/dcsm/solidipes/-/tree/main/examples/assets?ref_type=heads) 