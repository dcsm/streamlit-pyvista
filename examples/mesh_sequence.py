import streamlit as st

from streamlit_pyvista.mesh_viewer_component import MeshViewerComponent
from streamlit_pyvista.trame_viewers import get_advanced_viewer_path

import os


def list_files_in_folder(folder_path):
    if not os.path.isdir(folder_path):
        print("The provided folder path does not exist or is not a directory.")
        return []

    file_paths = []
    for root, _, files in os.walk(folder_path):
        for file in files:
            file_paths.append(os.path.join(root, file))

    return file_paths


def main():
    st.title("Streamlit-PyVista Example of mesh sequence")
    path_list = list_files_in_folder("assets/VTK")
    path_list = ["assets/results_corr-000000.vtk", "assets/results_corr-000019.vtk"]
    MeshViewerComponent(["assets/VTK/results_corr-000010.vtk"]+path_list[:1200],
                        trame_viewer_class=get_advanced_viewer_path()).show()


if __name__ == "__main__":
    main()
