import streamlit as st
from streamlit_pyvista.mesh_viewer_component import MeshViewerComponent

st.header("Pyvista Mesh Viewer App")

st.subheader("Component")
MeshViewerComponent("assets/Sequence/results_corr-000010.vtk").show()
