import os
import time
from pathlib import Path

import pytest
from playwright.sync_api import Page

from .e2e_utils import StreamlitRunner, find_free_port, image_similarity, get_iframe, \
    get_decimation_checkbox, save_mesh_in_a_file, get_wireframe_checkbox

DEFAULT_SIMPLE_MESH = "assets/mesh.png"
DEFAULT_MESH_WIREFRAME_1 = "assets/wireframe_1.png"
DEFAULT_MESH_WIREFRAME_0 = "assets/wireframe_0.png"
DEFAULT_MESH_U_FIELD_WIREFRAME_1 = "assets/u_field_wireframe_1.png"
DEFAULT_MESH_DECIMATED_1 = "assets/decimated_1.png"
DEFAULT_MESH_DECIMATED_0 = "assets/decimated_0.png"
DEFAULT_SLIDER_IMAGE = "assets/slider_image_reference.png"

SCRIPT_DIRECTORY = Path(__file__).parent.absolute()
APP_FILE = SCRIPT_DIRECTORY / "mesh_sequence_app.py"
# Set the environment variable
os.environ['STREAMLIT_PYVISTA_DECIMATION_THRESHOLD'] = '4000'
os.environ["STREAMLIT_PYVISTA_CACHE_TTL_MINUTES"] = "0"


@pytest.fixture(autouse=True, scope="module")
def streamlit_app():
    with StreamlitRunner(APP_FILE, server_port=find_free_port()) as runner:
        yield runner


@pytest.fixture(autouse=True, scope="function")
def go_to_app(page: Page, streamlit_app: StreamlitRunner):
    page.goto(streamlit_app.server_url)
    # Wait for app to load
    print("Waiting for app to load...")
    page.get_by_role("img", name="Running...").is_hidden()
    print("App loaded!")


@pytest.fixture(scope="function")
def check_mesh_loaded(page: Page):
    frame = get_iframe(page)
    test_path = "tmp/test_mesh_loaded.png"
    reference_path = DEFAULT_SIMPLE_MESH
    save_mesh_in_a_file(page, frame, test_path)
    res = image_similarity(test_path, reference_path)
    if res:
        os.remove(test_path)
    return res


def test_render_component(page: Page):
    frame = page.get_by_test_id("stIFrame").content_frame

    repr_dropdown = frame.get_by_test_id("representation_dropdown").nth(0)
    repr_dropdown.click()
    repr_dropdown.press("ArrowDown")
    repr_dropdown.press("ArrowDown")
    repr_dropdown.press("Enter")
    page.wait_for_timeout(1000)

    dec_checkbox = get_decimation_checkbox(frame)
    initial_state: bool = dec_checkbox.is_checked()
    dec_checkbox.click()
    assert dec_checkbox.is_checked() != initial_state


def test_mesh_loaded(check_mesh_loaded):
    assert check_mesh_loaded


@pytest.mark.xfail(reason="Play button click not working on ci runner.")
def test_sequence_start_slider(page: Page, check_mesh_loaded):
    if not check_mesh_loaded:
        pytest.skip("Skipping due to failure to load the mesh")

    frame = get_iframe(page)
    c = get_wireframe_checkbox(frame)
    if not c.is_checked():
        c.click()
    assert c.is_checked()
    page.wait_for_timeout(2000)
    slider_value_holder = frame.get_by_test_id("slider_value").first
    initial_value = int(slider_value_holder.inner_html())
    assert initial_value == 0

    play_button = frame.get_by_test_id("play_sequence_button").first
    play_button.click()

    page.wait_for_timeout(600)
    new_val = int(slider_value_holder.inner_html())
    assert initial_value <= new_val
    init_time = time.time()

    while int(slider_value_holder.inner_html()) < 7 and time.time() - init_time < 4:
        continue

    play_button.click()
    assert int(slider_value_holder.inner_html()) >= 7

    repr_dropdown = frame.get_by_test_id("representation_dropdown").nth(0)
    repr_dropdown.click()
    repr_dropdown.press("ArrowDown")
    repr_dropdown.press("ArrowDown")
    repr_dropdown.press("Enter")
    page.wait_for_timeout(1000)

    tmp_file = "tmp/slider_image.png"
    save_mesh_in_a_file(page, frame, tmp_file)
    assert image_similarity(tmp_file, DEFAULT_SLIDER_IMAGE)
    os.remove(tmp_file)


@pytest.mark.xfail(reason="Play button click not working on ci runner.")
def test_slider_change_sequence(page: Page, check_mesh_loaded):
    if not check_mesh_loaded:
        pytest.skip("Skipping due to failure to load the mesh")

    frame = get_iframe(page)
    c = get_wireframe_checkbox(frame)
    if not c.is_checked():
        c.click()
    assert c.is_checked()
    slider_value_holder = frame.get_by_test_id("slider_value").first
    initial_value = int(slider_value_holder.inner_html())
    assert initial_value == 0

    slider_comp = frame.get_by_test_id("sequence_slider_component").first
    slider_handle = slider_comp.locator("div.v-slider-thumb")
    slider_handle.hover(force=True, position={"x": 0, "y": 0})
    page.mouse.down()
    slider_handle.hover(force=True, position={"x": 400, "y": 0})
    page.mouse.up()
    assert int(slider_value_holder.inner_html()) == 7

    repr_dropdown = frame.get_by_test_id("representation_dropdown").nth(0)
    repr_dropdown.click()
    repr_dropdown.press("ArrowDown")
    repr_dropdown.press("ArrowDown")
    repr_dropdown.press("Enter")
    page.wait_for_timeout(1000)

    tmp_path = "tmp/slider_moved_image.png"
    save_mesh_in_a_file(page, frame, tmp_path)
    assert image_similarity(tmp_path, DEFAULT_SLIDER_IMAGE, threshold=6000)
    os.remove(tmp_path)
