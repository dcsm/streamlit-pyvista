import streamlit as st

from streamlit_pyvista.mesh_viewer_component import MeshViewerComponent

st.header("Pyvista Mesh Viewer App")

st.subheader("Component")
sequence = [
    "assets/Sequence/results_corr-000000.vtk",
    "assets/Sequence/results_corr-000001.vtk",
    "assets/Sequence/results_corr-000002.vtk",
    "assets/Sequence/results_corr-000003.vtk",
    "assets/Sequence/results_corr-000004.vtk",
    "assets/Sequence/results_corr-000005.vtk",
    "assets/Sequence/results_corr-000006.vtk",
    "assets/Sequence/results_corr-000007.vtk",
    "assets/Sequence/results_corr-000008.vtk",
    "assets/Sequence/results_corr-000009.vtk",
    "assets/Sequence/results_corr-000010.vtk",
]
MeshViewerComponent(sequence).show()
