import streamlit as st

from streamlit_pyvista.mesh_viewer_component import MeshViewerComponent
from streamlit_pyvista.trame_viewers import get_advanced_viewer_path

st.header("Pyvista Mesh Viewer App")

st.subheader("Component")
MeshViewerComponent("assets/Sequence/results_corr-000010.vtk",
                    trame_viewer_class=get_advanced_viewer_path()).show()
