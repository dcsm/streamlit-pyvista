import os
from pathlib import Path

import pytest
from playwright.sync_api import Page

from .e2e_utils import StreamlitRunner, find_free_port, image_similarity, get_iframe, get_wireframe_checkbox, \
    get_decimation_checkbox, save_mesh_in_a_file

DEFAULT_SIMPLE_MESH = "assets/mesh.png"
DEFAULT_MESH_WIREFRAME_1 = "assets/wireframe_1.png"
DEFAULT_MESH_WIREFRAME_0 = "assets/wireframe_0.png"
DEFAULT_MESH_U_FIELD_WIREFRAME_1 = "assets/u_field_wireframe_1.png"
DEFAULT_MESH_DECIMATED_1 = "assets/decimated_1.png"
DEFAULT_MESH_DECIMATED_0 = "assets/decimated_0.png"

SCRIPT_DIRECTORY = Path(__file__).parent.absolute()
APP_FILE = SCRIPT_DIRECTORY / "simple_app.py"
# Set the environment variable
os.environ['STREAMLIT_PYVISTA_DECIMATION_THRESHOLD'] = '4000'
os.environ["STREAMLIT_PYVISTA_CACHE_TTL_MINUTES"] = "0"


@pytest.fixture(autouse=True, scope="module")
def streamlit_app():
    with StreamlitRunner(APP_FILE, server_port=find_free_port()) as runner:
        yield runner


@pytest.fixture(autouse=True, scope="function")
def go_to_app(page: Page, streamlit_app: StreamlitRunner):
    page.goto(streamlit_app.server_url)
    # Wait for app to load
    print("Waiting for app to load...")
    page.get_by_role("img", name="Running...").is_hidden()
    print("App loaded!")
    page.wait_for_timeout(3000)


@pytest.fixture(scope="function")
def check_mesh_loaded(page: Page):
    frame = get_iframe(page)
    test_path = "tmp/test_mesh_loaded.png"
    reference_path = DEFAULT_SIMPLE_MESH
    save_mesh_in_a_file(page, frame, test_path)
    res = image_similarity(test_path, reference_path)
    if res:
        os.remove(test_path)
    return res


def test_render_component(page: Page):
    frame = page.get_by_test_id("stIFrame").content_frame

    repr_dropdown = frame.get_by_test_id("representation_dropdown").nth(0)
    repr_dropdown.click()
    repr_dropdown.press("ArrowDown")
    repr_dropdown.press("ArrowDown")
    repr_dropdown.press("Enter")
    page.wait_for_timeout(1000)

    dec_checkbox = get_decimation_checkbox(frame)
    initial_state: bool = dec_checkbox.is_checked()
    dec_checkbox.click()
    assert dec_checkbox.is_checked() != initial_state


def test_mesh_loaded(check_mesh_loaded):
    assert check_mesh_loaded


def test_wireframe_checkbox_exists_and_works(page: Page, check_mesh_loaded):
    if not check_mesh_loaded:
        pytest.skip("Skipping due to failure to load the mesh")
    frame = get_iframe(page)
    wf_checkbox = get_wireframe_checkbox(frame)
    initial_state: bool = wf_checkbox.is_checked()
    wf_checkbox.click()
    assert wf_checkbox.is_checked() != initial_state
    if wf_checkbox.is_checked():
        reference_path = DEFAULT_MESH_WIREFRAME_1
    else:
        reference_path = DEFAULT_MESH_WIREFRAME_0
    tmp_path = f"tmp/wireframe_{int(wf_checkbox.is_checked())}.png"
    save_mesh_in_a_file(page, frame, tmp_path)
    assert image_similarity(tmp_path, reference_path)
    os.remove(tmp_path)


def test_original_checkbox_exists_and_works(page: Page, check_mesh_loaded):
    if not check_mesh_loaded:
        pytest.skip("Skipping due to failure to load the mesh")

    frame = get_iframe(page)
    c = get_wireframe_checkbox(frame)
    if not c.is_checked():
        c.click()
    assert c.is_checked()
    checkbox = get_decimation_checkbox(frame)
    initial_state: bool = checkbox.is_checked()
    checkbox.click()
    assert checkbox.is_checked() != initial_state
    if checkbox.is_checked():
        reference_path = DEFAULT_MESH_DECIMATED_1
    else:
        reference_path = DEFAULT_MESH_DECIMATED_0
    page.wait_for_timeout(1500)
    tmp_path = f"tmp/decimated_{int(checkbox.is_checked())}.png"
    save_mesh_in_a_file(page, frame, tmp_path)
    assert image_similarity(tmp_path, reference_path)
    os.remove(tmp_path)


def test_representation_change_mesh(page: Page, check_mesh_loaded):
    if not check_mesh_loaded:
        pytest.skip("Skipping due to failure to load the mesh")

    frame = get_iframe(page)
    c = get_wireframe_checkbox(frame)
    if not c.is_checked():
        c.click()
    assert c.is_checked()
    result_ok = False
    nbr_trials = 0
    tmp_path = None
    while not result_ok and nbr_trials < 3:
        repr = frame.get_by_test_id("representation_dropdown").nth(0)
        repr.click()
        page.wait_for_timeout(500)
        repr.press("ArrowDown")
        repr.press("ArrowDown")
        repr.press("Enter")
        page.wait_for_timeout(1500)
        tmp_path = "tmp/test_u_field_decimated_wireframe.png"
        save_mesh_in_a_file(page, frame, tmp_path)
        result_ok = image_similarity(tmp_path, DEFAULT_MESH_U_FIELD_WIREFRAME_1, threshold=6000)
        nbr_trials += 1
    assert result_ok
    if tmp_path:
        os.remove(tmp_path)
