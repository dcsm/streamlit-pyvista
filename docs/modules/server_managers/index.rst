Server Managers
===============

.. toctree::
   :maxdepth: 1

   server_manager
   server_manager_proxified

The `server_managers` package contains all predefined server managers. A server manager is a class that is responsible
for launching Trame server instance that host the pyvista viewers.