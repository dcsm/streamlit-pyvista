Proxy
=====

.. automodule:: streamlit_pyvista.proxy
   :members:
   :undoc-members:
   :show-inheritance:

   This module implements proxy-related functionality for streamlit-pyvista. This Proxy is used to allow users to access
   trame server through a unique gateway, its particularly useful for use of the library in docker containers.