Trame Viewers
=============

.. toctree::
   :maxdepth: 1

   trame_backend
   trame_viewer
   trame_advance_viewer

A Trame viewer is a class that must implement `TrameBackend` and that while be a trame launched to display
interactive 3d viewer