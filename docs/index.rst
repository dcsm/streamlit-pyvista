.. streamlit-pyvista documentation master file, created by
   sphinx-quickstart on Fri Jul 19 09:29:15 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

streamlit-pyvista documentation
===============================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules/index

Indices and tables
==================
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`